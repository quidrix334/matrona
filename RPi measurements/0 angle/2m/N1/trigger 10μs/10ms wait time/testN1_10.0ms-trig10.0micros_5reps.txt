
Block: 1
======================================
200.198144007
200.674592948
200.690881801
200.674592948
200.674592948

Average distance is : 200.583 cm
Average deviation is : 0.215 cm
Block: 2
======================================
200.674592948
200.674592948
200.674592948
201.118464184
200.674592948

Average distance is : 200.763 cm
Average deviation is : 0.199 cm
Block: 3
======================================
200.194071794
200.674592948
200.230721712
200.247010565
200.230721712

Average distance is : 200.315 cm
Average deviation is : 0.202 cm
Block: 4
======================================
200.247010565
200.230721712
201.134753036
200.283660483
201.085886478

Average distance is : 200.596 cm
Average deviation is : 0.470 cm
Block: 5
======================================
200.658304095
201.134753036
201.13882525
200.690881801
200.658304095

Average distance is : 200.856 cm
Average deviation is : 0.256 cm
Block: 6
======================================
201.167330742
200.670520735
200.230721712
200.230721712
200.247010565

Average distance is : 200.509 cm
Average deviation is : 0.413 cm
Block: 7
======================================
200.263299417
200.230721712
200.214432859
200.226649499
200.247010565

Average distance is : 200.236 cm
Average deviation is : 0.019 cm
Block: 8
======================================
200.674592948
201.118464184
200.674592948
200.658304095
200.690881801

Average distance is : 200.763 cm
Average deviation is : 0.199 cm
Block: 9
======================================
201.118464184
200.674592948
200.214432859
200.230721712
200.226649499

Average distance is : 200.493 cm
Average deviation is : 0.400 cm
Block: 10
======================================
200.674592948
200.198144007
200.230721712
200.658304095
200.637943029

Average distance is : 200.480 cm
Average deviation is : 0.243 cm
Block: 1
======================================
200.5361377
200.043399906
200.519848847
200.519848847
200.059688759

Average distance is : 200.336 cm
Average deviation is : 0.260 cm
Block: 2
======================================
200.027111053
200.043399906
200.519848847
200.483198929
200.059688759

Average distance is : 200.227 cm
Average deviation is : 0.252 cm
Block: 3
======================================
200.059688759
200.503559995
200.027111053
200.931142378
200.503559995

Average distance is : 200.405 cm
Average deviation is : 0.374 cm
Block: 4
======================================
200.043399906
200.059688759
200.039327693
200.519848847
200.043399906

Average distance is : 200.141 cm
Average deviation is : 0.212 cm
Block: 5
======================================
200.519848847
200.02303884
200.039327693
200.043399906
200.059688759

Average distance is : 200.137 cm
Average deviation is : 0.214 cm
Block: 6
======================================
200.5361377
200.059688759
200.503559995
200.059688759
200.075977612

Average distance is : 200.247 cm
Average deviation is : 0.249 cm
Block: 7
======================================
200.080049825
200.499487782
200.092266464
200.552426553
200.5361377

Average distance is : 200.352 cm
Average deviation is : 0.244 cm
Block: 8
======================================
200.963720083
200.043399906
200.519848847
200.503559995
200.503559995

Average distance is : 200.507 cm
Average deviation is : 0.325 cm
Block: 9
======================================
200.980008936
199.632106376
199.59952867
200.039327693
200.075977612

Average distance is : 200.065 cm
Average deviation is : 0.557 cm
Block: 10
======================================
200.043399906
200.503559995
200.503559995
200.503559995
200.519848847

Average distance is : 200.415 cm
Average deviation is : 0.208 cm