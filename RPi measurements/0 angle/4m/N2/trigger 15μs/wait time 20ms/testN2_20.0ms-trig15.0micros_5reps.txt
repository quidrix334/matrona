
Block: 1
======================================
400.975731134
400.9433846
401.452842522
400.9433846
402.249375939

Average distance is : 401.313 cm
Average deviation is : 0.566 cm
Block: 2
======================================
401.367932868
403.114645743
401.808654404
401.808654404
403.130819011

Average distance is : 402.246 cm
Average deviation is : 0.820 cm
Block: 3
======================================
401.824827671
401.788437819
401.347716284
401.841000938
401.400279403

Average distance is : 401.640 cm
Average deviation is : 0.245 cm
Block: 4
======================================
401.436669254
400.959557867
401.028294253
400.991904402
401.432625937

Average distance is : 401.170 cm
Average deviation is : 0.243 cm
Block: 5
======================================
400.991904402
400.975731134
401.008077669
401.893564057
401.469015789

Average distance is : 401.268 cm
Average deviation is : 0.406 cm
Block: 6
======================================
401.873347473
401.469015789
401.893564057
401.448799205
401.008077669

Average distance is : 401.539 cm
Average deviation is : 0.365 cm
Block: 7
======================================
401.533708858
401.994646978
401.553925443
401.485189056
401.553925443

Average distance is : 401.624 cm
Average deviation is : 0.209 cm
Block: 8
======================================
401.469015789
401.501362324
401.464972472
401.028294253
401.028294253

Average distance is : 401.298 cm
Average deviation is : 0.247 cm
Block: 9
======================================
401.521578908
401.024250937
401.145550442
401.076814055
401.092987323

Average distance is : 401.172 cm
Average deviation is : 0.200 cm
Block: 10
======================================
401.076814055
401.57009871
400.656309104
400.63609252
401.113203907

Average distance is : 401.011 cm
Average deviation is : 0.385 cm