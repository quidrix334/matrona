
Block: 1
======================================
99.087100029
99.5331406593
99.0829700232
99.1201400757
99.136660099

Average distance is : 99.192 cm
Average deviation is : 0.192 cm
Block: 2
======================================
99.087100029
99.5372706652
99.5207506418
99.1201400757
99.136660099

Average distance is : 99.280 cm
Average deviation is : 0.228 cm
Block: 3
======================================
99.1201400757
98.3560889959
99.1201400757
99.1531801224
99.516620636

Average distance is : 99.053 cm
Average deviation is : 0.424 cm
Block: 4
======================================
99.5703107119
99.5001006126
99.087100029
99.136660099
99.1531801224

Average distance is : 99.289 cm
Average deviation is : 0.227 cm
Block: 5
======================================
99.6033507586
99.1862201691
99.5372706652
99.5207506418
99.136660099

Average distance is : 99.397 cm
Average deviation is : 0.218 cm
Block: 6
======================================
99.0994900465
98.5667192936
99.1036200523
99.136660099
99.1903501749

Average distance is : 99.019 cm
Average deviation is : 0.256 cm
Block: 7
======================================
99.1036200523
99.1036200523
99.136660099
99.1036200523
99.1160100698

Average distance is : 99.113 cm
Average deviation is : 0.014 cm
Block: 8
======================================
99.1036200523
99.136660099
68.0253261328
99.136660099
99.1201400757

Average distance is : 92.904 cm
Average deviation is : 13.908 cm
Block: 9
======================================
99.1160100698
99.5537906885
99.9998313189
99.087100029
99.0994900465

Average distance is : 99.371 cm
Average deviation is : 0.403 cm
Block: 10
======================================
99.136660099
99.087100029
70.3794294596
99.1201400757
99.136660099

Average distance is : 93.372 cm
Average deviation is : 12.853 cm
Average distance for 10x5 is : 97.999 cm
Average deviation for 10x5 is : 2.872 cm
 Percentage of values that failed: 0.000