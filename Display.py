class UI:

    def showAverage(self,average):
        print ("Average distance: " +str(average)+  " cm.")

    def showDeviation(self,deviation):
        print ("Deviation: "+str(deviation)+" cm.")

    def showCustomMessage(self,message):
        print (message)

    def showTotalAverageValues(self,avgDistance,avgDeviation,REPS):
        print('Average distance for 10x'+ str(REPS)+' is : %.3f cm' % avgDistance)
        print('Average deviation for 10x'+ str(REPS)+ ' is : %.3f cm' % avgDeviation)

    def showFinished(self):
        print("Finished")
